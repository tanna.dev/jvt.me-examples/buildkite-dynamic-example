package main

import (
	"fmt"
	"strings"
)

func main() {
	environments := []string{"staging", "production"}
	fmt.Println(strings.Join(environments, "\n"))
}
