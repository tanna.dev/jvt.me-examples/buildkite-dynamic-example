#!/usr/bin/env bash

cat <<EOF
steps:
EOF
while read -r environment; do
  cat <<EOF
  - label: ":k8s: Deploy to $environment environments"
    commands:
      - echo 'Doing the thing....'
EOF
done
